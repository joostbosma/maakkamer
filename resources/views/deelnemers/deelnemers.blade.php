<html>
<head>
    <title>beginsituatie</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="./web/stylesheets/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: -webkit-box;
        display: flex;
        -ms-flex-align: center;
        -ms-flex-pack: center;
        -webkit-box-align: center;
        align-items: center;
        -webkit-box-pack: center;
        justify-content: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }

    .form-signin {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .checkbox {
        font-weight: 400;
    }
    .form-signin .form-control {
        position: relative;
        box-sizing: border-box;
        height: auto;
        padding: 10px;
        font-size: 16px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
    }
    .btn {
        margin: 1.4%;
        width: 100%;
    }


</style>

<body>


<div class="text-center">
    <h1>Deelnemers</h1>
    <button onclick="location.href='{{ url('motorisch') }}'" type="button" class="btn btn-primary">Motorisch</button><br>
    <button onclick="location.href='{{ url ('cognitief') }}'" type="button" class="btn btn-primary">cognitief</button><br>
    <button onclick="location.href='{{ url('sociaalAfectief')}}'" type="button" class="btn btn-primary">Sociaal affectief</button><br>


</div>


{{-- <div class="deelnemers">
     <h1>Lesgevers</h1>
     <textarea placeholder="motorisch"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>

     <textarea placeholder="cognitief"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>

     <textarea placeholder="sociaal affectief"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>
 </div>

 <div class="lesgever">
     <h1>Deelnemers</h1>
     <textarea placeholder="algemeen"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>

     <textarea placeholder="motorisch"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>

     <textarea placeholder="cognitief"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>

     <textarea placeholder="sociaal affectief"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>

     <textarea placeholder="bijzonderheden"></textarea><br>
     <button type="button" class="btn btn-primary">Toevoegen</button><br>
 </div>--}}
</body>
</html>
