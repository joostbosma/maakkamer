


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Mike">

    <title>app</title>

    <link rel="stylesheet" href="./web/stylesheets/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            -ms-flex-align: center;
            -ms-flex-pack: center;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            padding-top: 40px;
            padding-bottom: 40px;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .checkbox {
            font-weight: 400;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        input:-webkit-autofill {
                     -webkit-box-shadow: 0 0 0px 1000px white inset;
                 }
    </style>
</head>
<body class="text-center">
    <form class="form-signin" method="post">
        <img class="mb-4" src="./web/images/landstede-logo.png" alt="">
        <h1 class="h3 mb-3 font-weight-normal">Sport en Bewegen</h1>
        <label for="inputEmail" class="sr-only">Gebruiker</label>
        <input type="text" id="inputEmail" class="form-control" name="username" placeholder="Gebruiker" required autofocus>
        <label for="inputPassword" class="sr-only">Wachtwoord</label>
        <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Wachtwoord" required>
        <button class="btn btn-lg btn-primary btn-block" name="login"  type="submit">Inloggen</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
    </form>
<script type="text/javascript" src="cordova.js"></script>
</body>
</html>
