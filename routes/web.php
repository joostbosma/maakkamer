<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inlog');
});
Route::get('menu', function (){
    return view('menu');
});
Route::get('organisator', function (){
    return view('organisator');
});
Route::get('begin', function (){
    return view('beginsituatie');
});
Route::get('gevolg', function (){
    return view('gevolg');
});
Route::get('leerdoel', function(){
     return view('persoonlijk');
});
Route::get('randvoorwaarden', function (){
    return view('randvoorwaarden');
});

Route::get('doelstellingen', function (){
    return view('doelstellingen');
});

// deelnemers

Route::get('deelnemers', function (){
    return view('deelnemers.deelnemers');
});

Route::get('motorisch', function (){
    return view('deelnemers.motorisch');
});

Route::get('cognitief', function (){
    return view('deelnemers.cognitief');
});

Route::get('sociaalAfectief', function (){
    return view('deelnemers.sociaalAfectief');
});

// lesgever

Route::get('lesgever', function (){
    return view('lesgever.lesgever');
});